data "aws_iam_policy_document" "demo_terraform_aws_access_rights" {
  statement {
      effect    = "Allow"
      actions   = ["ec2:*"]
      resources = ["*"]    
  }
}

resource "aws_iam_user" "demo_terraform_aws" {
  name = var.project
  path = "/"

  tags = {
    Name    = var.project
    Project = var.project
  }
}

resource "aws_iam_access_key" "demo_terraform_aws" {
  user = aws_iam_user.demo_terraform_aws.name
}

resource "aws_iam_user_policy" "demo_terraform_aws" {
  name = var.project
  user = aws_iam_user.demo_terraform_aws.name
  policy = data.aws_iam_policy_document.demo_terraform_aws_access_rights.json
}
