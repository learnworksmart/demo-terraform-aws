# demo-terraform-aws
This project managed an AWS environment using [GitLab managed Terraform State](https://docs.gitlab.com/ee/user/infrastructure/terraform_state.html#get-started-using-gitlab-ci) approach. 
  * Include static security scan on the Terraform code, using [tfsec](https://github.com/tfsec/tfsec)

## Setup. 
1. Create a new AWS user account, `cd prerequisites && terraform init && terraform apply --auto-approve`.
1. Take note of the new IAM user programmatic access, by running the following commands:
    * **AWS_ACCESS_KEY_ID**, `terraform state show aws_iam_access_key.demo_terraform_aws | grep id`
    * **AWS_SECRET_ACCESS_KEY**, `cat terraform.tfstate | grep \"secret\"\:`. The secret access key is stored in the terraform state file.
1. Set up your AWS credentials in your GitLab project's CI/CD Settings -> Variables.
    ![image](/uploads/2dc9e3ce79dca9b87e0065151ed0b7a2/image.png)
1. Run `git push`, observe the pipelines, manually trigger `deploy` state, and from the AWS console you should see an EC2 instance created.

## Key observations:
1. The gitlab-terraform runner cannot access the CI/CD protected variables. 
1. The `TF_VAR_xxx` variables are reference by `var.xxx` found in our terraform scripts.

## Unlock Terraform State 
1. [Creating a personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#creating-a-personal-access-token)
1. Run the following command:
  * `curl -X DELETE --header "PRIVATE-TOKEN: <personal access token>" https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/terraform/state/${CI_PROJECT_NAME}/lock`

## References: 
1. https://gitlab.com/gitlab-org/configure/examples/gitlab-terraform-aws/-/tree/master
1. https://docs.gitlab.com/ee/user/infrastructure/terraform_state.html#get-started-using-gitlab-ci
1. https://docs.gitlab.com/ee/ci/variables/predefined_variables.html
