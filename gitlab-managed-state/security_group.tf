resource "aws_security_group" "base_allow_default" {
  name        = var.project
  description = "Allow ssh and http(s) inbound traffic"

  tags = {
    Name = "${var.project}-allow-default"
    Project  = var.project
  }
}

resource "aws_security_group_rule" "base_ingress_port22" {
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["116.15.151.0/24"]
  security_group_id = aws_security_group.base_allow_default.id
  description       = "Allow ssh inbound traffic"
}

resource "aws_security_group_rule" "base_ingress_port80" {
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["116.15.151.0/24"]
  security_group_id = aws_security_group.base_allow_default.id
  description       = "Allow http inbound traffic"
}

resource "aws_security_group_rule" "base_egress_all" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["116.15.151.0/24"]
  security_group_id = aws_security_group.base_allow_default.id
  description       = "Allow https inbound traffic"
}
