resource "aws_instance" "ec2_inspec_scan_webserver" {
  # Amazon Linux 2 AMI (HVM), SSD Volume Type (64-bit x86)
  ami                    = "ami-015a6758451df3cb9"
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.base_allow_default.id]

  tags    = { 
    Name    = var.project
    OS      = "Amazon Linux 2 AMI"
    Project = var.project
  }
}
